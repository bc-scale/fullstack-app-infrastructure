# S3
resource "aws_s3_bucket" "media" {
  bucket        = "${var.project_name}-${terraform.workspace}-media"
  acl           = "public-read"
  force_destroy = true

  tags = {
    Name = "${var.project_name}-${terraform.workspace}-media"
  }
}

resource "aws_s3_bucket_policy" "media" {
  bucket = aws_s3_bucket.media.id

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "MYBUCKETPOLICY",
  "Statement": [
    {
      "Sid": "IPAllow",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::${aws_s3_bucket.media.id}/*"
    }
  ]
}
POLICY
}

resource "aws_s3_bucket_object" "worf" {
  bucket = aws_s3_bucket.media.id
  key    = "worf.jpg"
  source = "../../modules/s3/worf.jpg"
}