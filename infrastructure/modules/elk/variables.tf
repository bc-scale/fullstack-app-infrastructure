variable "aws_profile" {
  description = "aws profile"
  default     = "default"
}

variable "aws_amis" {
  default = {
    eu-west-1 = "ami-035966e8adab4aaad"
    us-west-2 = "ami-0d1cd67c26f5fca19"
    us-east-1 = "ami-07ebfd5b3428b6f4d"
  }
}

variable "elk_instance_type" {
  default = "t2.medium"
}

variable "aws_public_key_path" {
  default = "../../modules/elk/id_rsa.pub"
}

variable "aws_key_name" {
  description = "Name of the AWS key pair"
  default     = "klingon"
}

variable "elasticsearch_data_dir" {
  default = "/opt/elasticsearch/data"
}

variable "elasticsearch_cluster" {
  description = "Name of the elasticsearch cluster"
  default     = "elk_cluster"
}

variable "vpc_id" {
}

variable "private_subnet_1" {
}
variable "private_subnet_2" {
}
variable "public_subnet_1" {
}
variable "public_subnet_2" {
}

variable "aws_region" {
}
variable "bastion_security_group" {
}

variable "frontend_security_group" {
}

variable "loadbalancer_security_group" {
}

variable "project_name" {
}