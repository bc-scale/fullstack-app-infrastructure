resource "aws_security_group" "kibana" {
  name        = "elk_default_security_group"
  description = "Main security group for instances in public subnet"
  vpc_id      = var.vpc_id

  ingress {
    from_port       = 0
    to_port         = 65535
    protocol        = "tcp"
    security_groups = [var.bastion_security_group]
  }

  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["10.0.100.170/32"]
  }

  ingress {
    from_port       = 0
    to_port         = 65535
    protocol        = "tcp"
    security_groups = [var.loadbalancer_security_group]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.project_name}-${terraform.workspace}-ELK-default-security-group"
  }
}

resource "aws_security_group" "logstash" {
  name        = "elk_esearch_security_group"
  description = "Security group for Elasticsearch instance"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.project_name}-${terraform.workspace}-ELK-default-security-group"
  }
}

resource "aws_security_group" "elasticsearch" {
  name        = "elasticsearch_security_group"
  description = "Security group for elasticsearch cluster"
  vpc_id      = var.vpc_id

  # ingress {
  #   from_port = 9200
  #   to_port   = 9400
  #   protocol  = "tcp"
  #   security_groups = [var.frontend_security_group, var.bastion_security_group]
  # }

  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.project_name}-${terraform.workspace}-Elasticsearch-default-security-groups"
  }
}

resource "aws_iam_role" "elasticsearch" {
  name               = "elk_role"
  assume_role_policy = file("../../modules/elk/policies/role.json")
}

resource "aws_iam_role_policy" "elasticsearch" {
  name   = "${var.project_name}-${terraform.workspace}-elk_policy"
  policy = file("../../modules/elk/policies/policy.json")
  role   = aws_iam_role.elasticsearch.id
}

resource "aws_iam_instance_profile" "elasticsearch" {
  name = "${var.project_name}-${terraform.workspace}-elk_profile"
  path = "/"
  role = aws_iam_role.elasticsearch.name
}



# resource "aws_key_pair" "elk_auth" {
#   key_name   = var.aws_key_name
#   public_key = file(var.aws_public_key_path)
# }

data "template_file" "init_elasticsearch" {
  template = file("../../modules/elk/user_data/init_esearch.tpl")

  vars = {
    elasticsearch_cluster  = var.elasticsearch_cluster
    elasticsearch_data_dir = var.elasticsearch_data_dir
  }
}

resource "aws_instance" "elasticsearch" {
  ami                    = var.aws_amis[var.aws_region]
  instance_type          = var.elk_instance_type
  key_name               = var.project_name
  vpc_security_group_ids = [aws_security_group.elasticsearch.id]
  subnet_id              = var.private_subnet_1
  iam_instance_profile   = aws_iam_instance_profile.elasticsearch.id
  private_ip             = "10.0.100.170"

  ebs_block_device {
    device_name = "/dev/sdb"
    volume_type = "io1"
    volume_size = "20"
    iops        = "500"
  }

  user_data = data.template_file.init_elasticsearch.rendered

  volume_tags = {
    Name = "${var.project_name}-${terraform.workspace}-elasticsearch"
  }

  tags = {
    Name = "${var.project_name}-${terraform.workspace}-elasticsearch"
  }

  lifecycle {
    ignore_changes = [user_data]
  }
}

# data "template_file" "init_logstash" {
#   template = file("../../modules/elk/user_data/init_logstash.tpl")

#   vars = {
#     elasticsearch_host = aws_instance.elasticsearch.private_ip
#   }
# }

# resource "aws_instance" "logstash" {
#   ami                    = var.aws_amis[var.aws_region]
#   instance_type          = var.elk_instance_type
#   key_name               = var.aws_key_name
#   vpc_security_group_ids = [aws_security_group.logstash.id]
#   subnet_id              = var.private_subnet_1

#   ebs_block_device {
#     device_name = "/dev/sdb"
#     volume_type = "io1"
#     volume_size = "20"
#     iops        = "500"
#   }

#   user_data = data.template_file.init_logstash.rendered


#   tags = {
#     Name = "${var.project_name}-${terraform.workspace}-logstash"
#   }
# }

data "template_file" "init_kibana" {
  template = file("../../modules/elk/user_data/init_kibana.tpl")

  vars = {
    elasticsearch_host = aws_instance.elasticsearch.private_ip
  }
}

resource "aws_instance" "kibana" {
  ami                    = var.aws_amis[var.aws_region]
  instance_type          = var.elk_instance_type
  key_name               = var.project_name
  vpc_security_group_ids = [aws_security_group.kibana.id]
  subnet_id              = var.private_subnet_1

  ebs_block_device {
    device_name = "/dev/sdb"
    volume_type = "io1"
    volume_size = "10"
    iops        = "500"
  }

  user_data = data.template_file.init_kibana.rendered

  volume_tags = {
    Name = "${var.project_name}-${terraform.workspace}-kibana"
  }

  tags = {
    Name = "${var.project_name}-${terraform.workspace}-kibana"
  }

  lifecycle {
    ignore_changes = [user_data]
  }
}

resource "aws_iam_role" "kibana_dlm_lifecycle_role" {
  name = "${var.project_name}-${terraform.workspace}-kibana-dlm-lifecycle-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "dlm.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "kibana_dlm_lifecycle" {
  name = "${var.project_name}-${terraform.workspace}-kibana-dlm-lifecycle-policy"
  role = "${aws_iam_role.kibana_dlm_lifecycle_role.id}"

  policy = <<EOF
{
   "Version": "2012-10-17",
   "Statement": [
      {
         "Effect": "Allow",
         "Action": [
            "ec2:CreateSnapshot",
            "ec2:DeleteSnapshot",
            "ec2:DescribeVolumes",
            "ec2:DescribeSnapshots"
         ],
         "Resource": "*"
      },
      {
         "Effect": "Allow",
         "Action": [
            "ec2:CreateTags"
         ],
         "Resource": "arn:aws:ec2:*::snapshot/*"
      }
   ]
}
EOF
}

resource "aws_dlm_lifecycle_policy" "kibana" {
  description        = "${var.project_name}-${terraform.workspace}-kibana DLM lifecycle policy"
  execution_role_arn = "${aws_iam_role.kibana_dlm_lifecycle_role.arn}"
  state              = "ENABLED"

  policy_details {
    resource_types = ["VOLUME"]

    schedule {
      name = "2 weeks of daily snapshots"

      create_rule {
        interval      = 24
        interval_unit = "HOURS"
        times         = ["23:45"]
      }

      retain_rule {
        count = 14
      }

      tags_to_add = {
        SnapshotCreator = "DLM"
      }

      copy_tags = false
    }

    target_tags = {
      Name = "${var.project_name}-${terraform.workspace}-kibana"
    }
  }
}