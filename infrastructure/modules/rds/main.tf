resource "aws_db_instance" "rds" {
  allocated_storage    = var.allocated_storage
  storage_type         = var.storage_type
  engine               = var.engine
  instance_class       = var.instance_class
  identifier           = "${var.project_name}-${terraform.workspace}"
  username             = var.username
  password             = var.database_password
  deletion_protection  = false
  db_subnet_group_name = aws_db_subnet_group.db_subnet_group.id
  # final_snapshot_identifier   = "${var.project_name}-${terraform.workspace}"
  # snapshot_identifier         = false
  skip_final_snapshot         = true
  option_group_name           = var.option_group_name
  ca_cert_identifier          = "rds-ca-2019"
  license_model               = var.license_model
  multi_az                    = var.multi_az
  availability_zone           = var.availability_zone
  storage_encrypted           = var.storage_encrypted
  copy_tags_to_snapshot       = var.copy_tags_to_snapshot
  allow_major_version_upgrade = var.allow_major_version_upgrade
  auto_minor_version_upgrade  = var.auto_minor_version_upgrade
  apply_immediately           = var.apply_immediately
  maintenance_window          = var.maintenance_window
  backup_retention_period     = var.backup_retention_period
  backup_window               = var.backup_window
  vpc_security_group_ids      = [aws_security_group.rds.id]
  publicly_accessible         = false
  tags = {
    Name = "${var.project_name}-${terraform.workspace}-rds"
  }
}

resource "aws_db_subnet_group" "db_subnet_group" {
  name       = "${var.project_name}-${terraform.workspace}"
  subnet_ids = [var.private_subnet_1, var.private_subnet_2]

  tags = {
    Name = "${var.project_name}-${terraform.workspace}-DBSubnetGroup"
  }
}

resource "aws_security_group" "rds" {
  name        = "${var.project_name}-${terraform.workspace}-rds"
  description = "${var.project_name}-${terraform.workspace}-rds"
  vpc_id      = var.vpc_id

  ingress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = [var.frontend_security_group, var.bastion_security_group]
    description     = "Allows database connections"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.project_name}-${terraform.workspace}-RDS"
  }
}