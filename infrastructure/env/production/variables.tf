variable "project_name" {
}
variable "domain_name" {
}
variable "email" {
}

variable "aws_region" {
}
variable "az_count" {

}

# RDS
variable "database_password" {
}
variable "allocated_storage" {
}
variable "storage_type" {
}
variable "engine" {
}
variable "instance_class" {
}
variable "backup_retention_period" {
}

# variable "snapshot_identifier" {
# }

# ECS
variable "key_name" {
}

variable "instance_type" {
}

variable "asg_min" {
}

variable "asg_max" {
}

variable "asg_desired" {
}

variable "service_desired" {
}

# variable "google_analytics_credentials" {
# }

# variable "rails_master_key" {
# }

# variable "static_map_api_key" {
# }

# variable "google_maps_api_key" {
# }

# variable "google_analytics_tracking_id" {
# }
# variable "react_app_google_analytics_tracking_id" {
# }
variable "compress" {
}
variable "apply_immediately" {
}
variable "storage_encrypted" {
}
variable "username" {
}
variable "database_port" {
}